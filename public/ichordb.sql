-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jan 17, 2020 at 04:25 PM
-- Server version: 5.7.26
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `ichor_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `blood_types`
--

CREATE TABLE `blood_types` (
  `id` int(11) NOT NULL,
  `blood_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blood_types`
--

INSERT INTO `blood_types` (`id`, `blood_type`, `remarks`, `created_at`) VALUES
(1, '0+', 'Who can receive this type?\r\nO+, A+, B+, AB+', '2020-01-04 05:15:36'),
(2, '0-', 'Who can receive this type?\r\nAll blood types', '2020-01-04 05:15:36'),
(3, 'A+', 'Who can receive this type?\r\nA+, AB+', '2020-01-04 05:15:36'),
(4, 'A-', 'Who can receive this type?\r\nA+, A-, AB+, AB-', '2020-01-04 05:15:36'),
(5, 'B+', 'Who can receive this type?\r\nB+, AB+', '2020-01-04 05:15:36'),
(6, 'B-', 'Who can receive this type?\r\nB+, B-, AB+, AB-', '2020-01-04 05:15:36'),
(7, 'AB+', 'Who can receive this type?\r\nAB+', '2020-01-04 05:15:36'),
(8, 'AB-', 'Who can receive this type?\r\nAB+, AB-', '2020-01-04 05:15:36');

-- --------------------------------------------------------

--
-- Table structure for table `donations`
--

CREATE TABLE `donations` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `donnors`
--

CREATE TABLE `donnors` (
  `donnor_id` int(255) NOT NULL,
  `donnor_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `donnor_dob` date NOT NULL,
  `donnor_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `donnor_island` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `donnor_contact` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `donnor_sex` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `donnor_bloodtype` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `donnor_hb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `donnor_last_donation` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `id` int(11) NOT NULL,
  `name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `perm_id` int(11) NOT NULL,
  `perm_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `perm_desc` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`perm_id`, `perm_name`, `perm_desc`) VALUES
(1, 'donnors.browse', 'Browse Donnors'),
(2, 'donnors.read', 'Read Donnors'),
(3, 'donnors.edit', 'Edit Donnors'),
(4, 'donnors.add', 'Add Donnors');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(10) NOT NULL,
  `role_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role_group` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_perm`
--

CREATE TABLE `role_perm` (
  `id` int(11) NOT NULL,
  `role_id` int(10) NOT NULL,
  `perm_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `nid`, `password`, `user_role`, `designation`, `user_email`) VALUES
(1, 'Ahmed Sunil', 'A316319', 'Root@1234', 'Administrator', 'Developer', 'iahmednill@gmail.com'),
(2, 'Ibrahim Shaan', 'A331161', 'Root1234', 'Officer', 'Lab Assistant', 'shaan@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `user_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blood_types`
--
ALTER TABLE `blood_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donnors`
--
ALTER TABLE `donnors`
  ADD PRIMARY KEY (`donnor_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`perm_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `role_perm`
--
ALTER TABLE `role_perm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blood_types`
--
ALTER TABLE `blood_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `donnors`
--
ALTER TABLE `donnors`
  MODIFY `donnor_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `perm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role_perm`
--
ALTER TABLE `role_perm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
