<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| When you set this option to TRUE, it will replace ALL dashes in the
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'public/news';
$route['404_override'] = 'Error_notfound';
//$route['translate_uri_dashes'] = FALSE;
//$route['(:any)'] = 'pages/view/$1';


//Login Route
$route['login'] = 'Login_Controller';

//dashboard routes
$route['default_controller'] = 'Login_Controller';
$route['dashboard'] = 'dashboard_controller';

//user settings route
$route['settings'] = 'User_Setting_Controller';
$route['user_add'] = 'User_Add_Controller';
$route['user_view'] = 'Users_View_Controller';
$route['deleteuser/(:any)'] = 'User_Add_Controller/users_delete/$1';
$route['addjobs'] = 'Designations_Controller';
$route['submitjob'] = 'Designations_Controller/job_submit';
$route['deletejob/(:any)'] = 'Designations_Controller/job_delete/$1';


//blood types route
$route['bloodtype'] = 'Bloodtypes_Controller';
$route['addblood'] = 'Bloodtypes_Add_Controller';
$route['viewblood/(:any)'] = 'Bloodtypes_View_Controller/view_blood_by_id/$1';
$route['editblood/(:any)'] = 'Bloodtypes_Edit_Controller/edit_blood/$1';
$route['editbleed/(:any)'] = 'Bloodtypes_Edit_Controller/blood_edit/$1'; //edit blood(form submit function)
$route['submitblood'] = 'Bloodtypes_Add_Controller/blood_submit';
$route['deleteblood/(:any)'] = 'Bloodtypes_Add_Controller/blood_delete/$1';



//donnors routes
$route['donor_add'] = 'Donors_Add_Controller';
$route['donor_list'] = 'Donors_List_Controller';
$route['donor_available'] = 'Donors_Available_Controller';

//patients routes
$route['patients_add'] = 'Patients_Add_Controller';
$route['patients_list'] = 'Patients_List_Controller';
$route['patients_view'] = 'Patients_View_Controller';

//donations routes
$route['donations_add'] = 'Donations_Add_Controller';
$route['donations_list'] = 'Donations_List_Controller';
