<?php

class Patients_Add_Controller extends CI_Controller{
    public function __construct(){
        parent::__construct();
        //$this->load->model('patients_add_model');
    }
    
    public function index(){
        $data['title'] = "Patients Add";
        
        $this->load->view('templates/header');
        $this->load->view('patients/patients_add_view', $data);
        $this->load->view('templates/footer');
        
        
    }
}