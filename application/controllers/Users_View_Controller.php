<?php

class Users_View_Controller extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_setting_model');
	}

	public function index(){
		$data[]=[];
		$this->load->view('templates/header');
		$this->load->view('setting/user_view', $data);
		$this->load->view('templates/footer');
	}	
}
