<?php

class Bloodtypes_View_Controller extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('bloodtype_model');
    }
    
    public function view_blood_by_id($id){
		$data['bleed'] = $this->bloodtype_model->get_blood_id($id);
        $this->load->view('templates/header');
        $this->load->view('bloodtypes/blood_type_view' , $data);
        $this->load->view('templates/footer');
    }
}
