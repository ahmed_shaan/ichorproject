<?php

class Bloodtypes_Edit_Controller extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('bloodtype_model');
    }
    
    public function edit_blood($id){
		$data['bleed'] = $this->bloodtype_model->get_blood_id($id);
		$this->load->view('templates/header');
        $this->load->view('bloodtypes/blood_type_edit', $data);
        $this->load->view('templates/footer');
        
	}
	
	//to edit bloodtype
	public function blood_edit($id){
		$blood_array = array(
			"blood_type" => $this->input->post("bloodTypes"),
			"remarks" => $this->input->post("bloodRemarks")
		);
		$this->bloodtype_model->blood_update($id,$blood_array);
		$this->session->set_flashdata('success' ,'Blood Type has been updated');
		//redirect('editblood/'.$id);
		redirect('bloodtype');
		
	}

	
	
}


//to run the last query use
	// $str = $this->db->last_query();
		// 	echo "<pre>";
		// 	print_r($str);
		// 	exit;
