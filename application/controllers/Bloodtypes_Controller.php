<?php 

class Bloodtypes_Controller extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('bloodtype_model');
    }

    public function index(){
        $data['bloods'] = $this->bloodtype_model->get_all_types();
        $this->load->view('templates/header');
        $this->load->view('bloodtypes/bloodtypes_view', $data);
        $this->load->view('templates/footer');
    }
}
