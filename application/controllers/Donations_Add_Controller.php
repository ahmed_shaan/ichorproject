<?php

class Donations_Add_Controller extends CI_Controller{
    public function __construct(){
        parent::__construct();
        //$this->load->model('donations_add_model');
    }
    
    public function index(){
        $data['title'] = "Donations Add";
        
        $this->load->view('templates/header');
        $this->load->view('donations/donations_add_view', $data);
        $this->load->view('templates/footer');
        
        
    }
}