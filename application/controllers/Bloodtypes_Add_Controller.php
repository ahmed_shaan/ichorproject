<?php

class Bloodtypes_Add_Controller extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('bloodtype_model');
    }
	
	
    public function index(){
		//$data['js_links'] = 'js/apps/bloodtype.js';
        $this->load->view('templates/header');
        $this->load->view('bloodtypes/blood_type_add');
        $this->load->view('templates/footer');
	}

	//to submit data to blood type table
	public function blood_submit(){
		//checks for form validation
		$config_rules = array(
			array(
				"field" => "bloodTypes",
				"label" => "Blood Type",
				"rules" => "required|min_length[2]|trim|is_unique[blood_types.blood_type]"
			),
			array(
				"field" => "bloodRemarks",
				"label" => "Remarks",
				"rules" => "required|min_length[6]|trim|is_unique[blood_types.remarks]"
			),
		);

		$this->form_validation->set_rules($config_rules);

		if($this->form_validation->run() === FALSE){
			$this->index();
		}else{
			//to insert data to db
			$data = $this->input->post();
			$blood_array = array(
				"blood_type" => $data["bloodTypes"],
				"remarks" => $data["bloodRemarks"]
			);

			if($this->bloodtype_model->blood_submit($blood_array)){
				$this->session->set_flashdata("success", "Blood type has been created successfully");
				redirect('addblood');
			}else{
				$this->session->set_flashdata("error", "Failed to create Blood type");
				redirect('addblood');
			}	
		}
	}


	public function blood_delete($id){
		$this->bloodtype_model->blood_remove($id);
		// $str = $this->db->last_query();
		// 	echo "<pre>";
		// 	print_r($str);
		// 	exit;
		//set message before redirect
		$this->session->set_flashdata('deleted' ,'Bloodtype has been deleted');
		redirect('bloodtype');
	}
}
