<?php

	class Designations_Controller extends CI_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('user_setting_model');
		}
		
		public function index(){
			$data['jobs'] = $this->user_setting_model->get_jobs();
			$this->load->view('templates/header.php');
			$this->load->view('setting/add_jobs_view.php', $data);
			$this->load->view('templates/footer.php');
		}

		public function job_submit(){
			//checks for form validation
			$this->form_validation->set_rules('jobname', 'Designation', 'required|min_length[2]|trim|is_unique[designations.designation_name]');
			if($this->form_validation->run() === FALSE){
				$this->index();
			}else{
				//to insert data to db
				$data = $this->input->post();
				$job_array = array(
					"designation_name" => $data["jobname"]
				);
	
				if($this->user_setting_model->submit_job($job_array)){
					$this->session->set_flashdata("success", "Designation has been created successfully");
					redirect('addjobs');
				}else{
					$this->session->set_flashdata("error", "Failed to create Designation");
					redirect('addjobs');
				}	
			}
		}

		public function job_delete($id){
			$this->user_setting_model->job_remove($id);
			// $str = $this->db->last_query();
			// 	echo "<pre>";
			// 	print_r($str);
			// 	exit;
			//set message before redirect
			$this->session->set_flashdata('deleted' ,'Designation has been deleted');
			redirect('addjobs');
		}
	}
