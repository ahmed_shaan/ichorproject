<?php 

    class Error_notfound extends CI_Controller{
        public function __construct()
        {
            parent::__construct();
        }

        public function index(){
            $this->load->view("templates/header");
            $this->load->view("errors/404");
            $this->load->view("templates/footer");
        }
    }
