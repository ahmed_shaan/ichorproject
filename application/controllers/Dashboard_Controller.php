<?php
	
class Dashboard_Controller extends CI_Controller{
	public function __construct(){
		parent::__construct();
		//$this->load->model('dashboard_model');
	}
	
	public function index(){
		$this->load->view('templates/header.php');
		$this->load->view('dashboard_view.php');
		$this->load->view('templates/footer.php');
	}
}
	
	
?>