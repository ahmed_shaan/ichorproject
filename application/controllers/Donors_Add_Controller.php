<?php

class Donors_Add_Controller extends CI_Controller{
    public function __construct(){
        parent::__construct();
        //$this->load->model('donors_add_model');
    }
    
    public function index(){
        $data['title'] = "Add Donnor";
        
        $this->load->view('templates/header');
        $this->load->view('donors/donors_add_view', $data);
        $this->load->view('templates/footer');
        
        
    }
}