<?php

class Donations_List_Controller extends CI_Controller{
    public function __construct(){
        parent::__construct();
        //$this->load->model('donations_list_model');
    }
    
    public function index(){
        $data['title'] = "Donations List";
        
        $this->load->view('templates/header');
        $this->load->view('donations/donations_list_view', $data);
        $this->load->view('templates/footer');
        
        
    }
}