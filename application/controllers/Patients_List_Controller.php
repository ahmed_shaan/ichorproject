<?php

class Patients_List_Controller extends CI_Controller{
    public function __construct(){
        parent::__construct();
        //$this->load->model('patients_list_model');
    }
    
    public function index(){
        $data['title'] = "Patients List";
        
        $this->load->view('templates/header');
        $this->load->view('patients/patients_list_view', $data);
        $this->load->view('templates/footer');
        
        
    }
}