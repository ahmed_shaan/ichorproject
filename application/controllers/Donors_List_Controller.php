<?php

class Donors_List_Controller extends CI_Controller{
    public function __construct(){
        parent::__construct();
        //$this->load->model('donors_list_model');
    }
    
    public function index(){
        $data['title'] = "Donors List";
        
        $this->load->view('templates/header');
        $this->load->view('donors/donors_list_view', $data);
        $this->load->view('templates/footer');
        
        
    }
}