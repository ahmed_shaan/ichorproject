<?php

    class Donors_Available_Controller extends CI_Controller{
        public function __construct()
        {
            parent::__construct();
            $this->load->model('donors_list_model');
        }

        public function index(){
            $data[]='';
            $this->load->view('templates/header');
            $this->load->view('donors/donors_available_view');
            $this->load->view('templates/footer');
        }
    }