<?php

	class Patients_View_Controller extends CI_Controller {
		public function __construct()
		{
			parent::__construct();
			//$this->load->model('patients_list_model');
		}

		public function index(){
			$data[] = [];
			$this->load->view('templates/header');
			$this->load->view('patients/patients_view');
			$this->load->view('templates/footer');
		}
	}
