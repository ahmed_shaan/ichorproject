<?php

class User_Setting_Controller extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_setting_model');
    }

    public function index(){
        $data['users'] = $this->user_setting_model->get_users();
        // var_dump($data);
        // exit;
        $this->load->view('templates/header');
        $this->load->view('setting/user_setting_view', $data);
        $this->load->view('templates/footer');
    }




}