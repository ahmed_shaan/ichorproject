<?php

class User_Add_Controller extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_setting_model');
	}

	public function index(){
		$data[]='';
		$this->load->view('templates/header');
		$this->load->view('setting/add_user_view');
		$this->load->view('templates/footer');
	}
	
	//to submit users
	public function users_submit(){
		//checks for validation
		$config_rules = array(
			array(
				"field" => "name",
				"label" => "Name",
				"rules" => "required|min_length[2]|trim"
			),
			array(
				"field" => "nid",
				"label" => "National ID",
				"rules" => "required|min_length[6]|trim|is_unique[blood_types.blood_type]"
			),
			array(
				"field" => "contact",
				"label" => "Contact",
				"rules" => "required|min_length[6]|trim|is_unique[blood_types.blood_type]"
			),
			array(
				"field" => "password",
				"label" => "Password",
				"rules" => "required|min_length[4]|trim|"
			),
			array(
				"field" => "sex",
				"label" => "Sex",
				"rules" => "required"
			),
			array(
				"field" => "job",
				"label" => "Designation",
				"rules" => "required|min_length[3]|trim"
			),
			array(
				"field" => "role",
				"label" => "Role",
				"rules" => "required"
			),
			array(
				"field" => "email",
				"label" => "Email",
				"rules" => "required|min_length[6]|trim|is_unique[blood_types.blood_type]"
			)
		);

		$this->form_validation->set_rules($config_rules);
		if($this->form_validation->run() === FALSE){
			$this->index();
		}else{
			//Upload Image
			$config['upload_path'] = '../public/img/users';
			$config['allowed_types'] = 'png|jpg';
			$config['max_size'] = '2048';
			$config['max_width'] = '500';
			$config['max_height'] = '500';

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload()){
				$errors = array('error' => $this->upload->display_errors());
				$post_image = 'noimage.jpg';
			} else {
				$data = array('upload_data' => $this->upload->data());
				$post_image = $_FILES['pp']['name'];
			}

			//to insert data to db
			$data = $this->input->post();
			$users_array = array(
				"name" => $data["name"],
				"nid" => $data["nid"],
				"contact" => $data["contact"],
				"password" => $data["password"],
				"user_role" => $data["sex"],
				"designation" => $data["job"],
				"user_email" => $data["email"],
				"user_image" => $data["pp"]
			);

			if($this->user_setting_model->add_users($users_array, $post_image)){
				$this->session->set_flashdata("success", "User has been created successfully");
				redirect('settings');
			}else{
				$this->session->set_flashdata("error", "Failed to create User");
				redirect('settings');
			}	
		}
	}


	public function users_delete($id){
		$this->user_setting_model->delete_users($id);
		// $str = $this->db->last_query();
		// 	echo "<pre>";
		// 	print_r($str);
		// 	exit;
		//set message before redirect
		$this->session->set_flashdata('deleted' ,'User has been deleted');
		redirect('settings');
	}
}
