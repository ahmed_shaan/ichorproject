<?php

class bloodtype_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
	
	//to display all the information in blood_types table to the view
    public function get_all_types(){
        $query = $this->db->get('blood_types');
        return $result = $query->result_array();
	}
	
	//to get blood types by id
	public function get_blood_id($id){
		$this->db->select('*');
		$this->db->from('blood_types');
		$this->db->where('id',$id);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$data = $query->row();
			return $data;
		}
		return FALSE;
	}

	//insert blood types
	public function blood_submit($blood_array){
		return $this->db->insert("blood_types", $blood_array);
	}

	public function blood_update($id,$blood_array){	
		$this->db->update('blood_types', $blood_array, array('id' => $id));
	}
	
	public function blood_remove($id){
		$this->db->delete('blood_types' , array('id'=>$id));
	}

}
