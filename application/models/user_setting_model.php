<?php

class user_setting_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_users(){
        $query = $this->db->get('users');
        return $result = $query->result_array();
    }

    public function add_users(){
    }
	
	public function delete_users($id){
		$this->db->delete('users' , array('user_id'=>$id));
	}


	public function submit_job($job_array){
		return $this->db->insert("designations", $job_array);
	}


	public function get_jobs(){
        $query = $this->db->get('designations');
        return $result = $query->result_array();
	}

	
	public function job_remove($id){
		$this->db->delete('designations' , array('id'=>$id));
	}
}
