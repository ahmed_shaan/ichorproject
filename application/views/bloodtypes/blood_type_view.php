<div class="container addblood">
        <a href="/bloodtype" class="btn btn-danger btn-icon-split btn-sm">
            <span class="icon text-white-50">
                <i class="fas fa-angle-left"></i>
            </span>
            <span class="text">Back</span>
        </a>
        <h4 class="text-gray-600 text-center">Blood Type</h4>
        <hr>
        <div class="row mt-3">
        <div class="col-md-8 mb-4 mx-auto">
                    <div class="card border-left-danger shadow h-100 py-2">
                        <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Blood Type</div>
                            <div class="h1 mb-0 font-weight-bold text-gray-800 text-dangerpb"><?php echo $bleed->blood_type;?></div>
                            <!-- <h5>Remarks</h5> -->
							<br>
                			<p><?php echo $bleed->remarks;?></p>
                            </div>
                            <div class="col-auto">
                            <i class="fas fa-tint fa-4x text-danger"></i>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
				//TODO get data from donnors with the matching blood type
            <div class="col-md-7 mx-auto">
				<div class="container pt-4">
					<div class="col-md-6 mb-4 float-right">
						<div class="card border-left-danger shadow h-100 py-2">
							<div class="card-body">
							<div class="row no-gutters align-items-center">
								<div class="col mr-2">
								<div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Donnors</div>
								<div class="h5 mb-0 font-weight-bold text-gray-800">4</div>
								</div>
								<div class="col-auto">
								<i class="fas fa-users fa-2x text-gray-700"></i>
								</div>
							</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 mb-4">
						<div class="card border-left-danger shadow h-100 py-2">
							<div class="card-body">
							<div class="row no-gutters align-items-center">
								<div class="col mr-2">
								<div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Patients</div>
								<div class="h5 mb-0 font-weight-bold text-gray-800">5</div>
								</div>
								<div class="col-auto">
								<i class="fas fa-user-injured fa-2x text-gray-700"></i>
								</div>
							</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>   
</div>


