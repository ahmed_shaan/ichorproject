<div class="container addblood">
        <a href="<?php echo base_url();?>bloodtype" class="btn btn-danger btn-icon-split btn-sm">
            <span class="icon text-white-50">
                <i class="fas fa-angle-left"></i>
            </span>
            <span class="text">Back</span>
        </a>
        <h5 class="text-gray-600 text-center">Edit Blood Types</h5>
        <hr>
        <div class="container pt-4">
			<?php echo form_open('editbleed/'. $bleed->id);?>
				<div class="form-group">
					<label for="bloodType">Blood Type</label>
					<input type="text" value="<?php echo $bleed->blood_type;?>" name="bloodTypes" class="form-control" id="bloodtypes" placeholder="Blood type ">
					<?php echo form_error("bloodTypes" , "<div class='text-danger pl-1 pt-1' role='alert'>","</div>");?>
				</div>
				<div class="form-group">
					<label for="bloodtypeRemarks">Remarks</label>
					<input type="text" value="<?php echo $bleed->remarks;?>" name="bloodRemarks" class="form-control" id="typeremarks" placeholder="Remarks">
					<?php echo form_error("bloodRemarks" , "<div class='text-danger pl-1 pt-1' role='alert'>","</div>");?>
				</div>
				<button type="submit" class="btn btn-danger btn-icon-split mt-2">
					<span class="icon text-white-50">
								<i class="fas fa-save"></i>
					</span>
					<span class="text">Update</span>
				</button>
				<?php echo form_close();?>
		</div>
		<?php if($this->session->flashdata("success")) { ?>
			<div class="alert alert-success mt-4" role="alert">
				<?php echo $this->session->flashdata("success");?>
			</div>
		<?php } ?>
</div>
