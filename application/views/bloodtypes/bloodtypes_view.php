<div class="container">
        <a href="<?php echo base_url();?>addblood" class="btn btn-danger btn-icon-split btn-sm float-right">
            <span class="icon text-white-50">
                <i class="fas fa-tint"></i>
            </span>
            <span class="text">Add Type</span>
        </a>
        <h5 class="text-gray-600">Blood Types</h5>
        <hr>
    <table class="table table-sm align-middle table-striped table-bordered " width="100%" cellspacing="0"> 
        <thead class="bg-danger text-gray-100 text-center">
            <th>Blood Type</th>
            <th>Remarks</th>
            <th>Actions</th>
        </thead>
        <tbody class="bg-gray-200">
            <?php foreach($bloods as $blood):?>
                <tr>
                    <td class="text-center" style="vertical-align: middle;"><?php echo $blood['blood_type'];?></td>
                    <td style="vertical-align: middle;"><?php echo $blood['remarks'];?></td>
                    <td class="text-center" style="vertical-align: middle;">
                    <a href="<?php echo base_url('viewblood/'. $blood['id']);?>" class="btn btn-info btn-icon-split btn-sm">
                        <span class="icon text-white-100">
                        <i class="fas fa-eye"></i>
                        </span>
                        <span class="text">View</span>
                    </a> 
                    <a href="<?php echo base_url('editblood/'). $blood['id'];?>" class="btn btn-success btn-icon-split btn-sm">
                        <span class="icon text-white-100">
                        <i class="fas fa-edit"></i>
                        </span>
                        <span class="text">Edit</span>
										</a> 
										
                    <a href="<?php echo base_url('deleteblood/'. $blood['id']);?>" class="btn btn-danger btn-icon-split btn-sm" onclick="return confirm('Are you sure?');">
                        <span class="icon text-white-100">
                        <i class="fas fa-trash"></i>
                        </span>
                        <span class="text">Delete</span>
                    </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<!-- <br>
<br>
    <hr>
    <div class="row"> -->
    <?php //foreach($bloods as $blood):?>
        <!-- <div class="col-md-6">
        <table class="table table-bordered shadow dataTable" id="" width="100%" cellspacing="0" role="grid" aria-describedby="" style="width: 100%;">
                  <tbody>
                    <tr role="row" class="odd">
                      <td class="bloodyhell text-center" style="vertical-align: middle;">
                          <h2><?php //echo $blood['blood_type'];?></h2>
                      </td>
                      <td> -->
                          <!-- <h5>Remarks</h5> -->
                          <!-- <p><?php //echo $blood['remarks'];?></p>
                          <a href="#" class="btn btn-info btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                            <i class="fas fa-eye"></i>
                            </span>
                            <span class="text">View</span>
                        </a> 
                        <a href="#" class="btn btn-success btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                            <i class="fas fa-edit"></i>
                            </span>
                            <span class="text">Edit</span>
                        </a> 
                        <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                            <i class="fas fa-trash"></i>
                            </span>
                            <span class="text">Delete</span>
                        </a>
                      </td>
                    </tr>
                </tbody>
                </table>
        </div>
        <hr>
        <?php //endforeach; ?> -->
      </div>

      <!-- <div class="modal fade show" id="addbloodModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: block;" aria-modal="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div> -->
