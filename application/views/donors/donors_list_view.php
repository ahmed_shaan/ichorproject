<div class="table-responsive">
<h4>Donors</h4>
<hr>
<table class="table table-sm align-middle table-striped " id="dataTable" width="100%" cellspacing="0">
        <thead class="bg-danger text-gray-100 text-center">
            <th>Name</th>
            <th>National ID</th>
            <th>Sex</th>
            <th>Contact</th>
            <th>Blood Type</th>
            <th>Address</th>
            <th>Actions</th>
        </thead>
        <tbody class="bg-gray-200 text-center">
                <tr>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ahmed Sunil</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A316319</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Male</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">7626626</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A+</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Marine Villa</td>
                    <td class="text-center" style="vertical-align: middle;">
                    <a href="#" class="btn btn-info btn-icon-split btn-sm">
                        <span class="icon text-white-100">
                        <i class="fas fa-eye"></i>
                        </span>
                        <!-- <span class="text">View</span> -->
                    </a> 
                    <a href="#" class="btn btn-success btn-icon-split btn-sm">
                        <span class="icon text-white-100">
                        <i class="fas fa-edit"></i>
                        </span>
                        <!-- <span class="text">Edit</span> -->
                    </a> 
                    <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                        <span class="icon text-white-100">
                        <i class="fas fa-trash"></i>
                        </span>
                        <!-- <span class="text">Delete</span> -->
                    </a>
                    </td>
                </tr>
                <tr>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ahmed Sunil</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A316319</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Male</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">7626626</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A+</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Marine Villa</td>
                    <td class="text-center" style="vertical-align: middle;">
                    <a href="#" class="btn btn-info btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-eye"></i>
                        </span>
                        <!-- <span class="text">View</span> -->
                    </a> 
                    <a href="#" class="btn btn-success btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-edit"></i>
                        </span>
                        <!-- <span class="text">Edit</span> -->
                    </a> 
                    <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-trash"></i>
                        </span>
                        <!-- <span class="text">Delete</span> -->
                    </a>
                    </td>
                </tr>
                <tr>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ahmed Sunil</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A316319</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Male</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">7626626</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A+</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Marine Villa</td>
                    <td class="text-center" style="vertical-align: middle;">
                    <a href="#" class="btn btn-info btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-eye"></i>
                        </span>
                        <!-- <span class="text">View</span> -->
                    </a> 
                    <a href="#" class="btn btn-success btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-edit"></i>
                        </span>
                        <!-- <span class="text">Edit</span> -->
                    </a> 
                    <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-trash"></i>
                        </span>
                        <!-- <span class="text">Delete</span> -->
                    </a>
                    </td>
                </tr>
                <tr>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ahmed Sunil</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A316319</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Male</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">7626626</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A+</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Marine Villa</td>
                    <td class="text-center" style="vertical-align: middle;">
                    <a href="#" class="btn btn-info btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-eye"></i>
                        </span>
                        <!-- <span class="text">View</span> -->
                    </a> 
                    <a href="#" class="btn btn-success btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-edit"></i>
                        </span>
                        <!-- <span class="text">Edit</span> -->
                    </a> 
                    <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-trash"></i>
                        </span>
                        <!-- <span class="text">Delete</span> -->
                    </a>
                    </td>
                </tr>
                <tr>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ahmed Sunil</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A316319</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Male</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">7626626</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A+</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Marine Villa</td>
                    <td class="text-center" style="vertical-align: middle;">
                    <a href="#" class="btn btn-info btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-eye"></i>
                        </span>
                        <!-- <span class="text">View</span> -->
                    </a> 
                    <a href="#" class="btn btn-success btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-edit"></i>
                        </span>
                        <!-- <span class="text">Edit</span> -->
                    </a> 
                    <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-trash"></i>
                        </span>
                        <!-- <span class="text">Delete</span> -->
                    </a>
                    </td>
                </tr>
                <tr>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ahmed Sunil</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A316319</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Male</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">7626626</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A+</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Marine Villa</td>
                    <td class="text-center" style="vertical-align: middle;">
                    <a href="#" class="btn btn-info btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-eye"></i>
                        </span>
                        <!-- <span class="text">View</span> -->
                    </a> 
                    <a href="#" class="btn btn-success btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-edit"></i>
                        </span>
                        <!-- <span class="text">Edit</span> -->
                    </a> 
                    <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-trash"></i>
                        </span>
                        <!-- <span class="text">Delete</span> -->
                    </a>
                    </td>
                </tr>
                <tr>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ahmed Sunil</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A316319</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Male</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">7626626</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A+</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Marine Villa</td>
                    <td class="text-center" style="vertical-align: middle;">
                    <a href="#" class="btn btn-info btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-eye"></i>
                        </span>
                        <!-- <span class="text">View</span> -->
                    </a> 
                    <a href="#" class="btn btn-success btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-edit"></i>
                        </span>
                        <!-- <span class="text">Edit</span> -->
                    </a> 
                    <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-trash"></i>
                        </span>
                        <!-- <span class="text">Delete</span> -->
                    </a>
                    </td>
                </tr>
                <tr>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ahmed Sunil</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A316319</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Male</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">7626626</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A+</td>
                    <td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Marine Villa</td>
                    <td class="text-center" style="vertical-align: middle;">
                    <a href="#" class="btn btn-info btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-eye"></i>
                        </span>
                        <!-- <span class="text">View</span> -->
                    </a> 
                    <a href="#" class="btn btn-success btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-edit"></i>
                        </span>
                        <!-- <span class="text">Edit</span> -->
                    </a> 
                    <a href="#" class="btn btn-danger btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                        <i class="fas fa-trash"></i>
                        </span>
                        <!-- <span class="text">Delete</span> -->
                    </a>
                    </td>
                </tr>
        </tbody>
    </table>
</div>
