<a href="<?php echo base_url();?>patients_list" class="btn btn-danger btn-icon-split btn-sm">
	<span class="icon text-white-50">
		<i class="fas fa-angle-left"></i>
	</span>
	<span class="text">Back</span>
</a>
<h5 class="text-gray-600 text-center">Add Patient</h5>
<hr>
<br>
<div class="container">
    <form>
		<div class="form-row">
			<div class="col-md-4 col-sm-2">
				<label class="donnor-add-label" for="name">Name</label>
				<input type="text" class="form-control" placeholder="Enter Patient full name">
			</div>
			<div class="col-md-3 col-sm-2">
				<label class="donnor-add-label" for="nid">National ID</label>
				<input type="text" class="form-control" placeholder="Enter ID Card Number">
			</div>
			<div class="col-md-3 col-sm-2">
			<label class="donnor-add-label" for="nid">Contact</label>
				<input type="text" class="form-control" placeholder="Enter Contact Numbers">
			</div>
			<div class="col-md-2 col-sm-2">
				<label class="donnor-add-label" for="sex">Sex</label>
				<select id="sex" class="form-control">
					<option selected>Male</option>
					<option >Female</option>
					<option>Other</option>
				</select>
			</div>
		</div>
		<div class="form-row mt-4">
			<div class="col-md-4 col-sm-2">
					<label class="donnor-add-label" for="name">Address</label>
					<input type="text" class="form-control" placeholder="Enter Patient Address">
			</div>
			<div class="col-md-3 col-sm-2">
				<label class="donnor-add-label" for="nid">Island</label>
				<select id="sex" class="form-control">
					<option selected>Select Island</option>
					<option >R.Hulhudhuffaaru</option>
					<option>Other</option>
				</select>
			</div>
			<div class="col-md-3 col-sm-2">
				<label class="donnor-add-label" for="name">Date of Birth</label>
				<input type="date" class="form-control" placeholder="Enter Patient Date of Birth">
			</div>
			<div class="col-md-2 col-sm-2">
				<label class="donnor-add-label" for="sex">Blood Type</label>
				<select id="sex" class="form-control">
					<option selected>Blood Type</option>
					<option >A+</option>
					<option>O+</option>
				</select>
			</div>
		</div>
		<div class="form-row mt-4">
			<div class="col-md-4 col-sm-2">
				<label class="donnor-add-label" for="pp">Upload Photo</label>
				<div class="custom-file">
					<input type="file" class="custom-file-input" id="inputGroupFile02">
					<label class="custom-file-label" for="inputGroupFile02">Choose file</label>
				</div>
			</div>
		</div>
		<hr class="mt-4">
		<button type="submit" class="btn btn-danger btn-icon-split mt-2 float-right">
			<span class="icon text-white-50">
				<i class="fas fa-save"></i>
			</span>
			<span class="text">Save</span>
		</button>
    </form>
</div>
