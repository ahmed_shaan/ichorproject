<div class="table-responsive">
<h4>Donations</h4>
<hr>
<table class="table table-sm align-middle table-striped " id="dataTable" width="100%" cellspacing="0">
	<thead class="bg-danger text-gray-100 text-center thead-text">
	<tr>
	<th>Date</th>
	<th>Patient</th>
	<th>ABO & RH</th>
	<th>Donnor</th>
	<th>ABO & RH</th>
	<th>Actions</th>
	</tr>
	</thead>
	<tbody class="bg-gray-200 text-center">
	<tr>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">07/07/2019</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ali Saamin</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ahmed Sunil</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">A+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">
		<a href="#" class="btn btn-info btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-eye"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-success btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-edit"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-danger btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-trash"></i>
			</span>
		</a>
		</td>
	</tr>
	<tr>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">07/07/2019</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ibrahim Saamin</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Abdulla Saeed</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">
		<a href="#" class="btn btn-info btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-eye"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-success btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-edit"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-danger btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-trash"></i>
			</span>
		</a>
		</td>
	</tr>
	<tr>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">07/07/2019</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ibrahim Saamin</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Abdulla Saeed</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">
		<a href="#" class="btn btn-info btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-eye"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-success btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-edit"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-danger btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-trash"></i>
			</span>
		</a>
		</td>
	</tr>
	<tr>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">07/07/2019</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ibrahim Saamin</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Abdulla Saeed</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">
		<a href="#" class="btn btn-info btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-eye"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-success btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-edit"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-danger btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-trash"></i>
			</span>
		</a>
		</td>
	</tr>
	<tr>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">07/07/2019</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ibrahim Saamin</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Abdulla Saeed</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">
		<a href="#" class="btn btn-info btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-eye"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-success btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-edit"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-danger btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-trash"></i>
			</span>
		</a>
		</td>
	</tr>
	<tr>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">07/07/2019</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ibrahim Saamin</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Abdulla Saeed</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">
		<a href="#" class="btn btn-info btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-eye"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-success btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-edit"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-danger btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-trash"></i>
			</span>
		</a>
		</td>
	</tr>
	<tr>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">07/07/2019</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ibrahim Saamin</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Abdulla Saeed</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">
		<a href="#" class="btn btn-info btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-eye"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-success btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-edit"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-danger btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-trash"></i>
			</span>
		</a>
		</td>
	</tr>
	<tr>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">07/07/2019</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ibrahim Saamin</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Abdulla Saeed</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">
		<a href="#" class="btn btn-info btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-eye"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-success btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-edit"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-danger btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-trash"></i>
			</span>
		</a>
		</td>
	</tr>
	<tr>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">07/07/2019</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ibrahim Saamin</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Abdulla Saeed</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">
		<a href="#" class="btn btn-info btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-eye"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-success btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-edit"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-danger btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-trash"></i>
			</span>
		</a>
		</td>
	</tr>
	<tr>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">07/07/2019</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ibrahim Saamin</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Abdulla Saeed</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">
		<a href="#" class="btn btn-info btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-eye"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-success btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-edit"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-danger btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-trash"></i>
			</span>
		</a>
		</td>
	</tr>
	<tr>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">07/07/2019</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ibrahim Saamin</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Abdulla Saeed</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">
		<a href="#" class="btn btn-info btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-eye"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-success btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-edit"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-danger btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-trash"></i>
			</span>
		</a>
		</td>
	</tr>
	<tr>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">07/07/2019</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ibrahim Saamin</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Abdulla Saeed</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">
		<a href="#" class="btn btn-info btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-eye"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-success btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-edit"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-danger btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-trash"></i>
			</span>
		</a>
		</td>
	</tr>
	<tr>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">07/07/2019</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ibrahim Saamin</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Abdulla Saeed</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">
		<a href="#" class="btn btn-info btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-eye"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-success btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-edit"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-danger btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-trash"></i>
			</span>
		</a>
		</td>
	</tr>
	<tr>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">07/07/2019</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Ibrahim Saamin</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">Abdulla Saeed</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">O+</td>
		<td class="text-center text-gray-700 thead-text" style="vertical-align: middle;">
		<a href="#" class="btn btn-info btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-eye"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-success btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-edit"></i>
			</span>
		</a> 
		<a href="#" class="btn btn-danger btn-icon-split btn-sm">
			<span class="icon text-white-100">
			<i class="fas fa-trash"></i>
			</span>
		</a>
		</td>
	</tr>
	</tbody>
</table>
</div>
