<a href="<?php echo base_url();?>settings" class="btn btn-danger btn-icon-split btn-sm">
	<span class="icon text-white-50">
		<i class="fas fa-angle-left"></i>
	</span>
	<span class="text">Back</span>
</a><hr>
<div class="container">
<?php echo form_open_multipart('submitjob');?>
    <div class="form-row">
        <div class="col-md-6 col-sm-2">
			<label class="donnor-add-label" for="name">Designation</label>
			<div class="input-group">
			<input type="text" class="form-control" name="jobname" placeholder="Enter Designation">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-danger" type="button">Save</button>
			</span>
			</div>
			<?php echo form_error("jobname" , "<div class='text-danger pl-1 pt-1' role='alert'>","</div>");?>			
		</div>
    </div>
    <hr class="mt-4">
    <table class="table table-sm align-middle table-striped table-bordered " width="100%" cellspacing="0"> 
        <thead class="bg-danger text-gray-100 text-center">
            <th>Designations</th>
            <th>Actions</th>
        </thead>
        <tbody class="bg-gray-200">
            <?php foreach($jobs as $job):?>
                <tr>
                    <td class="text-left" style="vertical-align: center;"><?php echo $job['designation_name'];?></td>
                    <td class="text-right p5" style="vertical-align: center;">
                    <a href="<?php echo base_url('deletejob/'. $job['id']);?>" class="btn btn-danger btn-icon-split btn-sm" onclick="return confirm('Are you sure?');">
                        <span class="icon text-white-100">
                        <i class="fas fa-trash"></i>
                        </span>
                        <span class="text">Delete</span>
                    </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
	<?php echo form_close();?>
	<?php if($this->session->flashdata("success")) { ?>
			<div class="alert alert-success mt-4" role="alert">
				<?php echo $this->session->flashdata("success");?>
			</div>
		<?php } ?>
		<?php if($this->session->flashdata("deleted")) { ?>
			<div class="alert alert-success mt-4" role="alert">
				<?php echo $this->session->flashdata("deleted");?>
			</div>
		<?php } ?>
</div>

