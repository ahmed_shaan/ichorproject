<a href="<?php echo base_url();?>settings" class="btn btn-danger btn-icon-split btn-sm">
	<span class="icon text-white-50">
		<i class="fas fa-angle-left"></i>
	</span>
	<span class="text">Back</span>
</a>
	<h5 class="text-gray-600 text-center">Add User</h5>
<hr>
<br>
<div class="container">
<?php echo form_open_multipart('submit_user');?>
    <div class="form-row">
        <div class="col-md-3 col-sm-2">
            <label class="donnor-add-label" for="name">Name</label>
            <input type="text" class="form-control" name="name" placeholder="Enter full name">
        </div>
        <div class="col-md-3 col-sm-2">
            <label class="donnor-add-label" for="nid">National ID</label>
            <input type="text" name="nid" class="form-control" placeholder="Enter ID Card Number">
		</div>
		<div class="col-md-3 col-sm-2">
            <label class="donnor-add-label" for="nid">Password</label>
            <input type="text" name="password" class="form-control" placeholder="Enter a Password">
        </div>
        <div class="col-md-3 col-sm-2">
        	<label class="donnor-add-label" for="contact">Contact</label>
            <input type="text" name="contact" class="form-control" placeholder="Enter Contact Number">
        </div>
    </div>
    <div class="form-row mt-4">
	<div class="col-md-2 col-sm-2">
            <label class="donnor-add-label" for="sex">Sex</label>
            <select id="sex" class="form-control">
                <option selected>Male</option>
                <option >Female</option>
                <option>Other</option>
            </select>
        </div>
        <div class="col-md-3 col-sm-2">
        	<label class="donnor-add-label" for="job">Designation</label>
                <select id="jobs" class="form-control">
                    <option selected>Designation</option>
                    <option >Developer</option>
                    <option >Other</option>
                </select>
            </div>
        <div class="col-md-3 col-sm-2">
			<label class="donnor-add-label" for="role">Role</label>
			<select id="roles" class="form-control">
				<option selected>Select Role</option>
				<option >Administrator</option>
				<option>Other</option>
			</select>
        </div>
        <div class="col-md-4 col-sm-2">
                <label class="donnor-add-label" for="email">Email</label>
                <input type="text" name="email" class="form-control" placeholder="Enter Email Address">
        </div>
    </div>
    <div class="form-row mt-4">
        <div class="col-md-4 col-sm-2">
        	<label class="donnor-add-label" for="pp">Upload Photo</label>
			<div class="custom-file">
				<input type="file" name="pp" class="custom-file-input" id="inputGroupFile02">
				<label class="custom-file-label" for="inputGroupFile02">Choose file</label>
			</div>
        </div>
    </div>
    <hr class="mt-4">
    <button type="submit" class="btn btn-danger btn-icon-split mt-2 float-right">
		<span class="icon text-white-50">
			<i class="fas fa-save"></i>
		</span>
		<span class="text">Save</span>
    </button>
	<?php echo form_close();?>
</div>
