<div class="table-responsive">
    <h4 class="text-gray-600">Administration</h4>
    <hr>
        <a href="<?php echo base_url();?>user_add" class="btn btn-danger btn-icon-split btn-sm">
            <span class="icon text-white-50">
                <i class="fas fa-user"></i>
            </span>
            <span class="text">Add User</span>
        </a>
        <a href="#" class="btn btn-danger btn-icon-split btn-sm">
            <span class="icon text-white-50">
                <i class="fas fa-tag"></i>
            </span>
            <span class="text">User Roles</span>
        </a>
        <a href="#" class="btn btn-danger btn-icon-split btn-sm">
            <span class="icon text-white-50">
                <i class="fas fa-key"></i>
            </span>
            <span class="text">Permissions</span>
		</a>
		<a href="/addjobs" class="btn btn-danger btn-icon-split btn-sm">
            <span class="icon text-white-50">
                <i class="fas fa-user-md"></i>
            </span>
            <span class="text">Designation</span>
        </a>

    <hr>
    <h6>Users</h6>
    <table class="table table-sm align-middle table-striped table-bordered" width="100%" cellspacing="0">
        <thead class="bg-danger text-gray-100 text-center">
            <th>Name</th>
            <th>National ID</th>
            <th>Email</th>
            <th>User Role</th>
            <th>Designation</th>
            <th>Actions</th>
        </thead>
        <tbody class="bg-gray-200 text-center">
        <?php foreach($users as $user):?>
                <tr>
                    <td><?php echo $user['name'];?></td>
                    <td><?php echo $user['nid'];?></td>
                    <td><?php echo $user['user_email'];?></td>
                    <td><?php echo $user['user_role'];?></td>
                    <td><?php echo $user['designation'];?></td>
                    <td class="text-center" style="vertical-align: middle;">
                    <a href="/user_view" class="btn btn-info btn-icon-split btn-sm">
                        <span class="icon text-white-100">
                        <i class="fas fa-eye"></i>
                        </span>
                        <!-- <span class="text">View</span> -->
                    </a> 
                    <a href="#" class="btn btn-success btn-icon-split btn-sm">
                        <span class="icon text-white-100">
                        <i class="fas fa-edit"></i>
                        </span>
                        <!-- <span class="text">Edit</span> -->
                    </a> 
                    <a href="<?php echo base_url('deleteuser/'. $user['user_id']);?>" class="btn btn-danger btn-icon-split btn-sm">
                        <span class="icon text-white-100">
                        <i class="fas fa-trash"></i>
                        </span>
                        <!-- <span class="text">Delete</span> -->
                    </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
